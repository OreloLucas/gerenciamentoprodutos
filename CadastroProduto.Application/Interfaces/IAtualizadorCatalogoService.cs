﻿using CadastroProduto.Application.Models;
using CadastroProduto.Domain.Entities.Catalogo;
using CadastroProduto.Domain.Entities.Produtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Services
{
    public interface IAtualizadorCatalogoService
    {
        void AtualizaProduto(CatalogoViewModel produto);
        void ExcluirProduto(CatalogoViewModel produto);
        void AtualizaCategoria(CatalogoViewModel categoria);
        void AtualizaFornecedor(CatalogoViewModel fornecedor);
    }
}
