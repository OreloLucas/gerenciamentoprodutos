﻿using CadastroProduto.Application.Models;
using CadastroProduto.Domain.Entities.Produtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Services
{
    public interface ICategoriaService
    {
        ReturnType<List<Categoria>> ListAll();
        ReturnType<List<Categoria>> Lista(int pagina);
        ReturnType<Categoria> Adicionar(CategoriaViewModel produto);
        ReturnType<Categoria> Editar(CategoriaViewModel produto);
        ReturnType<Categoria> Excluir(CategoriaViewModel produto);
        ReturnType<Categoria> Visualizar(CategoriaViewModel produto);
    }
}
