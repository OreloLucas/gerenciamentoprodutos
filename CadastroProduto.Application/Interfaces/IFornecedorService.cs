﻿using CadastroProduto.Application.Models;
using CadastroProduto.Domain.Entities.Produtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Services
{
    public interface IFornecedorService
    {
        ReturnType<List<Fornecedor>> ListAll();
        ReturnType<List<Fornecedor>> Lista(int pagina);
        ReturnType<Fornecedor> Adicionar(FornecedorViewModel produto);
        ReturnType<Fornecedor> Editar(FornecedorViewModel produto);
        ReturnType<Fornecedor> Excluir(FornecedorViewModel produto);
        ReturnType<Fornecedor> Visualizar(FornecedorViewModel produto);
    }
}
