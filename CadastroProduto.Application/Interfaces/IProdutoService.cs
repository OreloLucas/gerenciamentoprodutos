﻿using CadastroProduto.Application.Models;
using CadastroProduto.Domain.Entities.Produtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Services
{
    public interface IProdutoService
    {
        ReturnType<List<Produto>> ListAll();
        ReturnType<List<Produto>> Lista(int pagina);
        ReturnType<Produto> Adicionar(ProdutoViewModel produto);
        ReturnType<Produto> Editar(ProdutoViewModel produto);
        ReturnType<Produto> Excluir(ProdutoViewModel produto);
        ReturnType<Produto> Visualizar(ProdutoViewModel produto); 
    }
}
