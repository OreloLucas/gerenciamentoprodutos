﻿using CadastroProduto.Application.Models;
using CadastroProduto.Domain.Entities.Catalogo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Interfaces
{
    public interface IReciveMessageService
    {
        ReturnType<Catalogo> AtualizaProduto(CatalogoViewModel produto);
        ReturnType<Catalogo> ExcluirProduto(CatalogoViewModel produto);
        ReturnType<Catalogo> AtualizaCategoria(CatalogoViewModel catalogo);
        ReturnType<Catalogo> AtualizaFornecedor(CatalogoViewModel fornecedor);

    }
}
