﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Interfaces
{
    public interface ISendMessageService
    {
        void SendMessage<T>(string queue, T obj);

    }
}
