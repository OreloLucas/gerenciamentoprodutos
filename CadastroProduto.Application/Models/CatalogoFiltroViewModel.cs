﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Models
{
    public class CatalogoFiltroViewModel
    {
        public int Pagina { get; set; }
        public int? IdProduto { get; set; }
        public string? DescricaoProduto { get; set; }
        public int? IdFornecedor { get; set; }
        public string? NomeFornecedor { get; set; }
        public int? IdCategoria { get; set; }
        public string? NomeCategoria { get; set; }
        public decimal? PrecoVenda{ get; set; }
    }
}
