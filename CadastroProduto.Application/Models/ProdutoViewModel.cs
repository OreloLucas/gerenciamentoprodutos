﻿using CadastroProduto.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Models
{
    public class ProdutoViewModel
    {
        public int Id { get; set; }
        public string? Descricao { get; set; }
        public int IdCategoria { get; set; }
        public TipoProduto Tipo { get; set; }
        public decimal Preco_venda { get; set; }
        public decimal Preco_custo { get; set; }
        public int IdFornecedor { get; set; }

        public string? NomeFornecedor { get; set; }
        public string? NomeCategoria { get; set; }
    }
}
