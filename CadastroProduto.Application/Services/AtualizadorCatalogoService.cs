﻿using CadastroProduto.Application.Models;
using CadastroProduto.Domain.Entities.Catalogo;
using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Interfaces.Produtos;
using CadastroProduto.Domain.Ws;
using CadastroProduto.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Services
{
    public class AtualizadorCatalogoService : IAtualizadorCatalogoService
    {
        private readonly IWsAtualizadorCatalogos _wsAtualizadorCatalogo;
        private readonly IFornecedorRepository _fornecedorRepository;
        private readonly ICategoriaRepository _categoriaRepository;

        public AtualizadorCatalogoService(IWsAtualizadorCatalogos wsAtualizadorCatalogo,
            IFornecedorRepository fornecedorRepository,
        ICategoriaRepository categoriaRepository)
        {
            _fornecedorRepository = fornecedorRepository;
            _categoriaRepository = categoriaRepository;
            _wsAtualizadorCatalogo = wsAtualizadorCatalogo;
        }

        public void AtualizaProduto(CatalogoViewModel produto)
        {            
            _wsAtualizadorCatalogo.AtualizaProduto(new Catalogo()
            {
                IdProduto = produto.IdProduto,
                Descricao = produto.Descricao,
                PrecoVenda = produto.PrecoVenda,
                IdCategoria = produto.IdCategoria,
                IdFornecedor = produto.IdFornecedor,
                NomeCategoria = produto.NomeCategoria == "" ? _categoriaRepository.Visualizar(produto.IdCategoria).Nome : produto.NomeCategoria,
                NomeFornecedor = produto.NomeFornecedor == "" ? _fornecedorRepository.Visualizar(produto.IdFornecedor).Nome : produto.NomeFornecedor
            });
        }

        public void ExcluirProduto(CatalogoViewModel produto)
        {
            _wsAtualizadorCatalogo.ExcluirProduto(new Catalogo()
            {
                IdProduto = produto.IdProduto,
            });
        }

        public void AtualizaCategoria(CatalogoViewModel categoria)
        {
            _wsAtualizadorCatalogo.AtualizaCategoria(new Catalogo()
            {
                IdCategoria = categoria.IdCategoria,
                NomeCategoria = categoria.NomeCategoria,

            });
        }

        public void AtualizaFornecedor(CatalogoViewModel fornecedor)
        {
            _wsAtualizadorCatalogo.AtualizaFornecedor(new Catalogo()
            {
                IdFornecedor = fornecedor.IdFornecedor,
                NomeFornecedor = fornecedor.NomeFornecedor,

            });
        }
    }
}
