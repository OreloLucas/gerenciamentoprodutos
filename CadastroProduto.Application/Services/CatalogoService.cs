﻿using CadastroProduto.Application.Models;
using CadastroProduto.Application.Services;
using CadastroProduto.Domain.Entities.Catalogo;
using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Interfaces.Produtos;

namespace CadastroCategoria.Application.Services
{
    public class CatalogoService : ICatalogoService
    {
        private readonly ICatalogoRepository _catalogoRepository;

        public CatalogoService(ICatalogoRepository catalogoRepository)
        {
            _catalogoRepository = catalogoRepository;
        }

        public ReturnType<List<Catalogo>> Lista(CatalogoFiltroViewModel CatViewModel)
        {
            var lista = _catalogoRepository.Lista(CatViewModel.Pagina, new Catalogo()
            {
                IdProduto = CatViewModel.IdProduto ?? 0,
                Descricao = CatViewModel.DescricaoProduto,
                IdCategoria = CatViewModel.IdCategoria ?? 0,
                NomeCategoria = CatViewModel.NomeCategoria, 
                IdFornecedor = CatViewModel.IdFornecedor ?? 0,
                NomeFornecedor = CatViewModel.NomeFornecedor,
                PrecoVenda = CatViewModel.PrecoVenda ?? 0,
            });
            return lista is null ? new ReturnType<List<Catalogo>>()
            {
                Mensagem = "Erro",
                Sucesso = false,
            } : new ReturnType<List<Catalogo>>()
            {
                Mensagem = "Ok",
                Sucesso = true,
                obj = lista,
            };
        }
    }
}
