﻿using CadastroProduto.Application.Interfaces;
using CadastroProduto.Application.Models;
using CadastroProduto.Application.Services;
using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Interfaces.Produtos;

namespace CadastroCategoria.Application.Services
{
    public class CategoriaService : ICategoriaService
    {
        private readonly ICategoriaRepository _categoriaRepository;
        private readonly ISendMessageService _sendMessage;

        public CategoriaService(ICategoriaRepository categoriaRepository,
            ISendMessageService sendMessage)
        {
            _categoriaRepository = categoriaRepository;
            _sendMessage = sendMessage;
        }
        private ReturnType<Categoria> ValidaCategoriaExiste(CategoriaViewModel categoria)
        {
            var p = Visualizar(categoria);
            if (!p.Sucesso)
            {
                return new ReturnType<Categoria>()
                {
                    Mensagem = "Categoria não localizado",
                    Sucesso = false,
                };
            }

            return new ReturnType<Categoria>()
            {
                Mensagem = "",
                Sucesso = true,
                obj = p.obj,
            };
        }

        public ReturnType<Categoria> Editar(CategoriaViewModel categoria)
        {
            var c = ValidaCategoriaExiste(categoria);
            if (!c.Sucesso)
                return c;

            _categoriaRepository.Detached(c.obj);
            var p = new Categoria()
            {
                Id = categoria.Id,
                Nome = categoria.Nome
            };

            if (_categoriaRepository.Editar(p))
            {
                _sendMessage.SendMessage("atualiza-categoria", new CatalogoViewModel()
                {
                    NomeCategoria = p.Nome,
                    IdCategoria = p.Id,
                });
                return new ReturnType<Categoria>()
                {
                    Mensagem = "Alterado com sucesso",
                    Sucesso = true,
                    obj = p,
                };
            }

            return new ReturnType<Categoria>()
            {
                Mensagem = "Erro Alterando",
                Sucesso = false,
                obj = p,
            };
        }

        public ReturnType<Categoria> Excluir(CategoriaViewModel categoria)
        {
            var r = ValidaCategoriaExiste(categoria);
            if (!r.Sucesso)
                return new ReturnType<Categoria>()
                {
                    Mensagem = r.Mensagem,
                    Sucesso = r.Sucesso,
                };

            if (r.obj.GetProdutos().Count > 0)
                return new ReturnType<Categoria>()
                {
                    Mensagem = "Categoria Possui produto vinculado",
                    Sucesso = true,
                };

            var deletou = _categoriaRepository.Excluir(r.obj);
            return deletou ? new ReturnType<Categoria>()
            {
                Mensagem = "Excluido com sucesso",
                Sucesso = true,
            } : new ReturnType<Categoria>()
            {
                Mensagem = "Erro ao excluir",
                Sucesso = false,
            };
        }

        public ReturnType<List<Categoria>> Lista(int pagina)
        {
            var prod = _categoriaRepository.Lista(pagina);
            return prod is null ? new ReturnType<List<Categoria>>()
            {
                Mensagem = "Erro",
                Sucesso = false,
            } : new ReturnType<List<Categoria>>()
            {
                Mensagem = "Ok",
                Sucesso = true,
                obj = prod,
            };
        }

        public ReturnType<List<Categoria>> ListAll()
        {
            var prod = _categoriaRepository.ListAll();
            return prod is null ? new ReturnType<List<Categoria>>()
            {
                Mensagem = "Erro",
                Sucesso = false,
            } : new ReturnType<List<Categoria>>()
            {
                Mensagem = "Ok",
                Sucesso = true,
                obj = prod,
            };
        }

        public ReturnType<Categoria> Adicionar(CategoriaViewModel categoria)
        {
            var p = new Categoria()
            {
                Nome = categoria.Nome
            };

            var prod = _categoriaRepository.Adicionar(p);
            return prod is null ? new ReturnType<Categoria>()
            {
                Mensagem = "Erro",
                Sucesso = false,
            } : new ReturnType<Categoria>()
            {
                Mensagem = "Adicionado com sucesso",
                Sucesso = true,
                obj = prod,
            };
        }

        public ReturnType<Categoria> Visualizar(CategoriaViewModel categoria)
        {
            var prod = _categoriaRepository.Visualizar(categoria.Id);
            return prod is null ? new ReturnType<Categoria>()
            {
                Mensagem = "Erro",
                Sucesso = false,
            } : new ReturnType<Categoria>()
            {
                Mensagem = "Ok",
                Sucesso = true,
                obj = prod,
            };
        }
    }
}
