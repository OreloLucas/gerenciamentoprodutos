﻿using CadastroProduto.Application.Interfaces;
using CadastroProduto.Application.Models;
using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Interfaces.Produtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Services
{
    public class FornecedorService : IFornecedorService
    {
        private readonly IFornecedorRepository _fornecedorRepository;
        private readonly ISendMessageService _sendMessage;

        public FornecedorService(IFornecedorRepository fornecedorRepository,
            ISendMessageService sendMessage)
        {
            _fornecedorRepository = fornecedorRepository;
            _sendMessage = sendMessage;
        }
        private ReturnType<Fornecedor> ValidaFornecedorExiste(FornecedorViewModel Fornecedor)
        {
            var p = Visualizar(Fornecedor);
            if (p is null)
            {
                return new ReturnType<Fornecedor>()
                {
                    Mensagem = "Fornecedor não localizado",
                    Sucesso = false,
                };
            }
            
            return new ReturnType<Fornecedor>()
            {
                Mensagem = "",
                Sucesso = true,
                obj = p.obj,
            };
        }

        public ReturnType<Fornecedor> Editar(FornecedorViewModel Fornecedor)
        {
            var r = ValidaFornecedorExiste(Fornecedor);
            if (!r.Sucesso)
                return r;
            _fornecedorRepository.Detached(r.obj);

            var p = new Fornecedor()
            {
                Id = Fornecedor.Id,
                Nome = Fornecedor.Nome
            };

            if (_fornecedorRepository.Editar(p))
            {
                _sendMessage.SendMessage("atualiza-fornecedor", new CatalogoViewModel()
                {
                    NomeFornecedor = p.Nome,
                    IdFornecedor = p.Id,
                });
                return new ReturnType<Fornecedor>()
                {
                    Mensagem = "Alterado com sucesso",
                    Sucesso = true,
                    obj = p,
                };
            }

            return new ReturnType<Fornecedor>()
            {
                Mensagem = "Erro Alterando",
                Sucesso = false,
                obj = p,
            };

            //return _fornecedorRepository.Editar(p) ?
            //new ReturnType<Fornecedor>()
            //{
            //    Mensagem = "Alterado com sucesso",
            //    Sucesso = true,
            //    obj = p,
            //} : new ReturnType<Fornecedor>()
            //{
            //    Mensagem = "Erro Alterando",
            //    Sucesso = false,
            //    obj = p,
            //};
        }

        public ReturnType<Fornecedor> Excluir(FornecedorViewModel Fornecedor)
        {
            var f = ValidaFornecedorExiste(Fornecedor);
            if (!f.Sucesso)
                return new ReturnType<Fornecedor>()
                {
                    Mensagem = f.Mensagem,
                    Sucesso = f.Sucesso,
                };

            if (f.obj.GetProdutos().Count > 0)
                return new ReturnType<Fornecedor>()
                {
                    Mensagem = "Fornecedor Possui produto vinculado",
                    Sucesso = true,
                };

            var deletou = _fornecedorRepository.Excluir(f.obj);
            return deletou ? new ReturnType<Fornecedor>()
            {
                Mensagem = "Excluido com sucesso",
                Sucesso = true,
            } : new ReturnType<Fornecedor>()
            {
                Mensagem = "Erro ao excluir",
                Sucesso = false,
            };
        }

        public ReturnType<List<Fornecedor>> Lista(int pagina)
        {
            var prod = _fornecedorRepository.Lista(pagina);
            return prod is null ? new ReturnType<List<Fornecedor>>()
            {
                Mensagem = "Erro",
                Sucesso = false,
            } : new ReturnType<List<Fornecedor>>()
            {
                Mensagem = "Ok",
                Sucesso = true,
                obj = prod,
            };
        }

        public ReturnType<List<Fornecedor>> ListAll()
        {
            var prod = _fornecedorRepository.ListAll();
            return prod is null ? new ReturnType<List<Fornecedor>>()
            {
                Mensagem = "Erro",
                Sucesso = false,
            } : new ReturnType<List<Fornecedor>>()
            {
                Mensagem = "Ok",
                Sucesso = true,
                obj = prod,
            };
        }

        public ReturnType<Fornecedor> Adicionar(FornecedorViewModel Fornecedor)
        {
            var p = new Fornecedor()
            {
                Nome = Fornecedor.Nome
            };

            var prod = _fornecedorRepository.Adicionar(p);
            return prod is null ? new ReturnType<Fornecedor>()
            {
                Mensagem = "Erro",
                Sucesso = false,
            } : new ReturnType<Fornecedor>()
            {
                Mensagem = "Adicionado com sucesso",
                Sucesso = true,
                obj = prod,
            };
        }

        public ReturnType<Fornecedor> Visualizar(FornecedorViewModel Fornecedor)
        {
            var prod = _fornecedorRepository.Visualizar(Fornecedor.Id);
            return prod is null ? new ReturnType<Fornecedor>()
            {
                Mensagem = "Erro",
                Sucesso = false,
            } : new ReturnType<Fornecedor>()
            {
                Mensagem = "Ok",
                Sucesso = true,
                obj = prod,
            };
        }
    }
}
