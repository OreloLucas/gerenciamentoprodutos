﻿using CadastroProduto.Application.Interfaces;
using CadastroProduto.Application.Models;
using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Interfaces.Produtos;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Services
{
    public class ProdutoService : IProdutoService
    {
        private readonly IProdutoRepository _produtoRepository;
        private readonly ICategoriaRepository _categoriaRepository;
        private readonly IFornecedorRepository _fornecedorRepository;
        private readonly ISendMessageService _sendMessage;

        public ProdutoService(IProdutoRepository produtoRepository,
            ICategoriaRepository categoriaRepository,
            IFornecedorRepository fornecedorRepository,
            ISendMessageService sendMessage)
        {
            _produtoRepository = produtoRepository;
            _categoriaRepository = categoriaRepository;
            _fornecedorRepository = fornecedorRepository;
            _sendMessage = sendMessage;
        }

        private ReturnType<Produto> ValidaProdutoExiste(ProdutoViewModel produto)
        {
            var p = Visualizar(produto);
            if (p is null)
            {
                return new ReturnType<Produto>()
                {
                    Mensagem = "Produto não localizado",
                    Sucesso = false,
                };
            }
            _produtoRepository.Detached(p.obj);
            return new ReturnType<Produto>()
            {
                Mensagem = "",
                Sucesso = true,
                obj = p.obj,
            };
        }

        private ReturnType<Produto> ValidaRegrasProduto(Produto produto)
        {
            var result = new ProdutoValidatior().Validate(produto);
            return result.IsValid ?
                new ReturnType<Produto>()
                {
                    Mensagem = "Ok",
                    Sucesso = true,
                } : new ReturnType<Produto>()
                {
                    Mensagem = result.Errors.First().ErrorMessage,
                    Sucesso = false,
                };
        }

        public ReturnType<Produto> Editar(ProdutoViewModel produto)
        {
            var r = ValidaProdutoExiste(produto);
            if (!r.Sucesso)
                return r;

            var p = new Produto()
            {
                Id = produto.Id,
                Descricao = produto.Descricao,
                IdFornecedor = produto.IdFornecedor,
                IdCategoria = produto.IdCategoria,
                Tipo = produto.Tipo,
                Preco_custo = produto.Preco_custo,
                Preco_venda = produto.Preco_venda,
                Fornecedor = _fornecedorRepository.Visualizar(produto.IdFornecedor),
                Categoria = _categoriaRepository.Visualizar(produto.IdCategoria),
            };

            var validation = ValidaRegrasProduto(p);

            //return !validation.Sucesso ? validation : _produtoRepository.Editar(p) ?
            //new ReturnType<Produto>()
            //{
            //    Mensagem = 
            //    Sucesso = true,
            //    obj = p,
            //} : new ReturnType<Produto>()
            //{
            //    Mensagem = "Erro Alterando",
            //    Sucesso = false,
            //    obj = p,
            //};

            if (!validation.Sucesso)
            {
                return validation;
            }
            else if (_produtoRepository.Editar(p))
            {
                _sendMessage.SendMessage("atualiza-produto", new CatalogoViewModel()
                {
                    IdProduto = p.Id,
                    Descricao = p.Descricao,
                    IdCategoria = p.IdCategoria,
                    IdFornecedor = p.IdFornecedor,
                    NomeFornecedor = p.Fornecedor.Nome,
                    NomeCategoria = p.Categoria.Nome,
                    PrecoVenda = p.Preco_venda,
                });
                return new ReturnType<Produto>()
                {
                    Mensagem = "Produto alterado com sucesso",
                    Sucesso = true,
                    obj = p,
                };
            }

            return new ReturnType<Produto>()
            {
                Mensagem = "Erro Alterando",
                Sucesso = false,
                obj = p,
            };
        }

        public ReturnType<Produto> Excluir(ProdutoViewModel produto)
        {
            var r = ValidaProdutoExiste(produto);
            if (!r.Sucesso)
                return new ReturnType<Produto>()
                {
                    Mensagem = r.Mensagem,
                    Sucesso = r.Sucesso,
                };

            var deletou = _produtoRepository.Excluir(r.obj);
            if (deletou)
            {
                _sendMessage.SendMessage("exclui-produto", new CatalogoViewModel()
                {
                    IdProduto = produto.Id,
                });
                return new ReturnType<Produto>()
                {
                    Mensagem = "Excluido com sucesso",
                    Sucesso = true,
                };
            }
            return new ReturnType<Produto>()
            {
                Mensagem = "Erro ao excluir",
                Sucesso = false,
            };
        }

        public ReturnType<List<Produto>> Lista(int pagina)
        {
            var prod = _produtoRepository.Lista(pagina);
            return prod is null ? new ReturnType<List<Produto>>()
            {
                Mensagem = "Erro",
                Sucesso = false,
            } : new ReturnType<List<Produto>>()
            {
                Mensagem = "Ok",
                Sucesso = true,
                obj = prod,
            };
        }

        public ReturnType<List<Produto>> ListAll()
        {
            var prod = _produtoRepository.ListAll();
            return prod is null ? new ReturnType<List<Produto>>()
            {
                Mensagem = "Erro",
                Sucesso = false,
            } : new ReturnType<List<Produto>>()
            {
                Mensagem = "Ok",
                Sucesso = true,
                obj = prod,
            };
        }

        public ReturnType<Produto> Adicionar(ProdutoViewModel produto)
        {
            var p = new Produto()
            {
                Descricao = produto.Descricao,
                IdFornecedor = produto.IdFornecedor,
                IdCategoria = produto.IdCategoria,
                Preco_custo = produto.Preco_custo,
                Preco_venda = produto.Preco_venda,
                Tipo = produto.Tipo,
                Fornecedor = _fornecedorRepository.Visualizar(produto.IdFornecedor),
                Categoria = _categoriaRepository.Visualizar(produto.IdCategoria),
            };

            var validation = ValidaRegrasProduto(p);

            if (!validation.Sucesso)
                return validation;

            var prod = _produtoRepository.Adicionar(p);
            if (prod is null)
            {
                return new ReturnType<Produto>()
                {
                    Mensagem = "Erro",
                    Sucesso = false,
                };
            }
            else
            {
                _sendMessage.SendMessage("atualiza-produto", new CatalogoViewModel()
                {
                    IdProduto = prod.Id,
                    Descricao = prod.Descricao,
                    IdCategoria = prod.IdCategoria,
                    IdFornecedor = prod.IdFornecedor,
                    NomeFornecedor = prod.Fornecedor.Nome,
                    NomeCategoria = prod.Categoria.Nome,
                    PrecoVenda = prod.Preco_venda,
                });
                return new ReturnType<Produto>()
                {
                    Mensagem = "Adicionado com sucesso",
                    Sucesso = true,
                    obj = prod,
                };
            }
        }

        public ReturnType<Produto> Visualizar(ProdutoViewModel produto)
        {
            var prod = _produtoRepository.Visualizar(produto.Id);
            return prod is null ? new ReturnType<Produto>()
            {
                Mensagem = "Erro",
                Sucesso = false,
            } : new ReturnType<Produto>()
            {
                Mensagem = "Ok",
                Sucesso = true,
                obj = prod,
            };
        }
    }
}
