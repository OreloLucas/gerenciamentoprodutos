﻿using CadastroProduto.Application.Interfaces;
using CadastroProduto.Application.Models;
using CadastroProduto.Domain.Entities.Catalogo;
using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Interfaces.Produtos;
using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Services
{
    public class ReciveMessageService : IReciveMessageService
    {
        private readonly ICatalogoRepository _catalogoRepository;

        public ReciveMessageService(ICatalogoRepository catalogoRepository)
        {
            _catalogoRepository = catalogoRepository;
        }

        public ReturnType<Catalogo> AtualizaCategoria(CatalogoViewModel categoria)
        {
            try
            {
                var cat = _catalogoRepository.GetCategoria(new Catalogo()
                {
                    IdCategoria = categoria.IdCategoria
                });
                foreach (var it in cat)
                {
                    it.NomeCategoria = categoria.NomeCategoria;
                    _catalogoRepository.AtualizaCatalogo(it);
                }
                return new ReturnType<Catalogo>()
                {
                    Mensagem = "Ok",
                    Sucesso = true,
                };
            }
            catch (Exception)
            {
                return new ReturnType<Catalogo>()
                {
                    Mensagem = "Erro",
                    Sucesso = false,
                };
            }
        }

        public ReturnType<Catalogo> AtualizaFornecedor(CatalogoViewModel fornecedor)
        {
            try
            {
                var cat = _catalogoRepository.GetFornecedores(new Catalogo()
                {
                    IdFornecedor = fornecedor.IdFornecedor
                });
                foreach (var it in cat)
                {
                    it.NomeFornecedor = fornecedor.NomeFornecedor;
                    _catalogoRepository.AtualizaCatalogo(it);
                }
                return new ReturnType<Catalogo>()
                {
                    Mensagem = "Ok",
                    Sucesso = true,
                };
            }
            catch (Exception)
            {
                return new ReturnType<Catalogo>()
                {
                    Mensagem = "Erro",
                    Sucesso = false,
                };
            }
        }

        public ReturnType<Catalogo> AtualizaProduto(CatalogoViewModel produto)
        {
            try
            {
                _catalogoRepository.AtualizaProduto(new Catalogo()
                {
                    IdProduto = produto.IdProduto,
                    Descricao = produto.Descricao,
                    IdCategoria = produto.IdCategoria,
                    IdFornecedor = produto.IdFornecedor,
                    PrecoVenda = produto.PrecoVenda,
                    NomeCategoria = produto.NomeCategoria,
                    NomeFornecedor = produto.NomeFornecedor,
                });
                return new ReturnType<Catalogo>()
                {
                    Mensagem = "Ok",
                    Sucesso = true,
                };
            }
            catch (Exception)
            {
                return new ReturnType<Catalogo>()
                {
                    Mensagem = "Erro",
                    Sucesso = false,
                };
            }
        }

        public ReturnType<Catalogo> ExcluirProduto(CatalogoViewModel produto)
        {
            try
            {
                _catalogoRepository.ExcluirProduto(new Catalogo()
                {
                    IdProduto = produto.IdProduto,
                });
                return new ReturnType<Catalogo>()
                {
                    Mensagem = "Ok",
                    Sucesso = true,
                };
            }
            catch (Exception)
            {
                return new ReturnType<Catalogo>()
                {
                    Mensagem = "Erro",
                    Sucesso = false,
                };
            }
        }
    }
}
