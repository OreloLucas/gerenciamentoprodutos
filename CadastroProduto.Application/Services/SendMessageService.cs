﻿using CadastroProduto.Application.Interfaces;
using CadastroProduto.Application.Models;
using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Interfaces.Produtos;
using FluentValidation.Results;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Application.Services
{
    public class SendMessageService : ISendMessageService
    {
        private readonly ConnectionFactory _factory;

        public SendMessageService()
        {
            _factory = new ConnectionFactory()
            {
                HostName = "localhost",
            };
        }

        public void SendMessage<T>(string queue, T obj)
        {
            var con = _factory.CreateConnection();
            var channel = con.CreateModel();
            channel.QueueDeclare(queue: queue,
                durable: true,
                exclusive: false,
                autoDelete: false,
                arguments: null);

            var stringfiedMessage = JsonConvert.SerializeObject(obj);
            var byteMessage = Encoding.UTF8.GetBytes(stringfiedMessage);

            channel.BasicPublish(exchange: "",
                routingKey: queue,
                basicProperties: null,
                body: byteMessage);
        }
    }
}
