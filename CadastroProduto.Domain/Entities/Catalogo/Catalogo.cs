﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Domain.Entities.Catalogo
{
    public class Catalogo
    {
        public int Id { get; set; }
        public int IdProduto { get; set; }
        public string? Descricao { get; set; }
        public int IdFornecedor { get; set; }
        public string? NomeFornecedor { get; set; }
        public int IdCategoria { get; set; }
        public string? NomeCategoria { get; set; }
        public decimal PrecoVenda { get; set; } 
    }
}
