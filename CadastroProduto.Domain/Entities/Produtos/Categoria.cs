﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Domain.Entities.Produtos
{
    public class Categoria
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        protected virtual List<Produto> Produtos { get; set; } = new List<Produto>();

        public List<Produto> GetProdutos() => Produtos;
    }
}
