﻿using CadastroProduto.Domain.Enum;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Domain.Entities.Produtos
{
    public class Produto
    {


        public int Id { get; set; }
        public string? Descricao { get; set; }
        public int IdCategoria { get; set; }
        public TipoProduto Tipo { get; set; }
        public decimal Preco_venda { get; set; }
        public decimal Preco_custo { get; set; }
        public int IdFornecedor { get; set; }


        public virtual Fornecedor Fornecedor { get; set; }
        public virtual Categoria Categoria { get; set; }
    }

    public class ProdutoValidatior : AbstractValidator<Produto>
    {
        public ProdutoValidatior()
        {
            RuleFor(x=>x.Tipo).Must(ValidaEnum).WithMessage("Tipo de produto inválido");
            RuleFor(x => x.Fornecedor).NotNull().WithMessage("Fornecedor não localizado");
            RuleFor(x => x.Categoria).NotNull().WithMessage("Categoria não localizado");
        }

        private bool ValidaEnum(TipoProduto tipo)
        {
            return System.Enum.IsDefined(typeof(TipoProduto), tipo);
        }

    }
}
