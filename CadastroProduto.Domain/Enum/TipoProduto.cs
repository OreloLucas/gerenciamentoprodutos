﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Domain.Enum
{
    public enum TipoProduto : short
    {
        MateriaPrima = 0,
        ProdutoAcabado = 1,
    }
}
