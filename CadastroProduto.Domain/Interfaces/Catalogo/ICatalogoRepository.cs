﻿using CadastroProduto.Domain.Entities.Catalogo;
using CadastroProduto.Domain.Entities.Produtos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Domain.Interfaces.Produtos
{
    public interface ICatalogoRepository
    {
        List<Catalogo>? Lista(int pagina, Catalogo cat); 
        void AtualizaProduto(Catalogo catalogo);
        void ExcluirProduto(Catalogo catalogo);
        List<Catalogo> GetFornecedores(Catalogo catalogo);
        List<Catalogo> GetCategoria(Catalogo catalogo);

        void AtualizaCatalogo(Catalogo catalogo); 
    }
}
