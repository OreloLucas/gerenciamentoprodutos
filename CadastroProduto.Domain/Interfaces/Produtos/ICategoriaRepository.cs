﻿using CadastroProduto.Domain.Entities.Produtos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Domain.Interfaces.Produtos
{
    public interface ICategoriaRepository
    {
        List<Categoria>? ListAll();
        List<Categoria>? Lista(int pagina);
        Categoria? Adicionar(Categoria produto);
        bool Editar(Categoria produto);
        bool Excluir(Categoria produto);
        Categoria? Visualizar(int id);
        void Detached(Categoria p);
    }
}
