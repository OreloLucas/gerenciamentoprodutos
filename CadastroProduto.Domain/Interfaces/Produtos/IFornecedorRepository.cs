﻿using CadastroProduto.Domain.Entities.Produtos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Domain.Interfaces.Produtos
{
    public interface IFornecedorRepository
    {
        List<Fornecedor>? ListAll();
        List<Fornecedor>? Lista(int pagina);
        Fornecedor? Adicionar(Fornecedor produto);
        bool Editar(Fornecedor produto);
        bool Excluir(Fornecedor produto);
        Fornecedor? Visualizar(int id);
        void Detached(Fornecedor p);
    }
}
