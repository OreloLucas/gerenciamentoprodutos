﻿using CadastroProduto.Domain.Entities.Produtos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Domain.Interfaces.Produtos
{
    public interface IProdutoRepository
    {
        List<Produto>? ListAll();
        List<Produto>? Lista(int pagina);
        Produto? Adicionar(Produto produto);
        bool Editar(Produto produto);
        bool Excluir(Produto produto);
        Produto? Visualizar(int id);
        void Detached(Produto p);
        


    }
}
