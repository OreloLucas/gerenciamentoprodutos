﻿
using CadastroProduto.Domain.Entities.Catalogo;

namespace CadastroProduto.Domain.Ws
{
    public interface IWsAtualizadorCatalogos
    {
        bool AtualizaProduto(Catalogo produto);
        bool ExcluirProduto(Catalogo produto);
        bool AtualizaCategoria(Catalogo categoria);
        bool AtualizaFornecedor(Catalogo fornecedor); 
    }
}
