﻿using CadastroProduto.Domain.Entities.Catalogo;
using CadastroProduto.Infra.Mapping.CatalogoProduto;
using Microsoft.EntityFrameworkCore;

namespace CadastroProduto.Infra.Context
{
    public class CatalogoContext : DbContext
    {
        public bool lazyload = false;

        public CatalogoContext(DbContextOptions<CatalogoContext> options) : base(options)
        { 
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new CatalogoMapping()); 
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies(lazyload);

        }

        public DbSet<Catalogo> Catalogo { get; set; } 

    }
}
