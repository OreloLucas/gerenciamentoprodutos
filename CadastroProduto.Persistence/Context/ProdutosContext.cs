﻿using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Infra.Mapping.Produtos;
using Microsoft.EntityFrameworkCore;

namespace CadastroProduto.Infra.Context
{
    public class ProdutosContext : DbContext
    {
        public bool lazyload = false;

        public ProdutosContext(DbContextOptions<ProdutosContext> options) : base(options)
        { 
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new ProdutoMapping());
            builder.ApplyConfiguration(new FornecedorMapping());
            builder.ApplyConfiguration(new CategoriaMapping());
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies(lazyload);

        }

        public DbSet<Produto> Produto { get; set; }
        public DbSet<Fornecedor> Fornecedor { get; set; }
        public DbSet<Categoria> Categoria { get; set; }

    }
}
