﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using CadastroProduto.Domain.Entities.Catalogo;

namespace CadastroProduto.Infra.Mapping.CatalogoProduto
{
    public class CatalogoMapping : IEntityTypeConfiguration<Catalogo>
    {
        public void Configure(EntityTypeBuilder<Catalogo> builder)
        {
            builder.ToTable("catalogo");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Descricao).HasColumnName("descricao").HasColumnType("varchar");
            builder.Property(x => x.IdProduto).HasColumnName("idproduto").HasColumnType("int");
            builder.Property(x => x.IdFornecedor).HasColumnName("idfornecedor").HasColumnType("int");
            builder.Property(x => x.NomeFornecedor).HasColumnName("nomefornecedor").HasColumnType("varchar"); 
            builder.Property(x => x.IdCategoria).HasColumnName("idcategoria").HasColumnType("int");
            builder.Property(x => x.NomeCategoria).HasColumnName("nomecategoria").HasColumnType("varchar");
            builder.Property(x => x.PrecoVenda).HasColumnName("precovenda").HasColumnType("money");
        }
    }
}
