﻿using CadastroProduto.Domain.Entities.Produtos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Infra.Mapping.Produtos
{
    public class CategoriaMapping : IEntityTypeConfiguration<Categoria>
    {
        public void Configure(EntityTypeBuilder<Categoria> builder)
        {
            builder.ToTable("categoria");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Nome).HasColumnName("nome").HasColumnType("varchar");

            //builder.HasMany("Produtos").WithOne("Categoria").HasForeignKey("IdCategoria");

        }
    }
}
