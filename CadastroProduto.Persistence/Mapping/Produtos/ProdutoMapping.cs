﻿using CadastroProduto.Domain.Entities.Produtos;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Infra.Mapping.Produtos
{
    public class ProdutoMapping : IEntityTypeConfiguration<Produto>
    {
        public void Configure(EntityTypeBuilder<Produto> builder)
        {
            builder.ToTable("produto");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Descricao).HasColumnName("descricao").HasColumnType("varchar");
            builder.Property(x => x.IdCategoria).HasColumnName("categoria").HasColumnType("smallint");
            builder.Property(x => x.Tipo).HasColumnName("tipo").HasColumnType("smallint");
            builder.Property(x => x.Preco_venda).HasColumnName("preco_venda").HasColumnType("money");
            builder.Property(x => x.Preco_custo).HasColumnName("preco_custo").HasColumnType("money");
            builder.Property(x => x.IdFornecedor).HasColumnName("id_fornecedor").HasColumnType("int");

            builder.HasOne(x => x.Fornecedor).WithMany("Produtos").HasForeignKey(x => x.IdFornecedor);
            builder.HasOne(x => x.Categoria).WithMany("Produtos").HasForeignKey(x => x.IdCategoria);

            //builder.HasOne(x => x.Fornecedor).WithMany().HasForeignKey(x=>x.IdFornecedor);
            //builder.HasOne(x => x.Categoria).WithMany().HasForeignKey(x=>x.IdCategoria);
        }
    }
}
