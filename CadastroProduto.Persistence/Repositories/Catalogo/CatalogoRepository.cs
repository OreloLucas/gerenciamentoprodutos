﻿using CadastroProduto.Domain.Entities.Catalogo;
using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Interfaces.Produtos;
using CadastroProduto.Infra.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Infra.Repositories.Produtos
{
    public class CatalogoRepository : ICatalogoRepository
    {
        private readonly CatalogoContext _context;

        public CatalogoRepository(CatalogoContext context)
        {
            _context = context;
        }

        public void AtualizaProduto(Catalogo catalogo)
        {
            var cat = ExisteProduto(catalogo.IdProduto);

            if (cat is null)
                _context.Add(catalogo);
            else
            {
                catalogo.Id = cat.Id;
                _context.Entry(cat).State = EntityState.Detached;
                _context.Update(catalogo);
            }


            _context.SaveChanges();
        }

        private Catalogo ExisteProduto(int idProduto)
            => _context.Catalogo.Where(x => x.IdProduto == idProduto).FirstOrDefault();

        public List<Catalogo>? Lista(int pagina, Catalogo cat)
        {
            try
            {
                _context.lazyload = false;
                IQueryable<Catalogo> result = _context.Catalogo;

                if (cat.IdProduto > 0)
                    result = result.Where(x => x.IdProduto == cat.IdProduto);
                if (cat.IdFornecedor > 0)
                    result = result.Where(x => x.IdFornecedor == cat.IdFornecedor);
                if (cat.IdCategoria > 0)
                    result = result.Where(x => x.IdCategoria == cat.IdCategoria);

                if (cat.Descricao is not null && !cat.Descricao.Equals(""))
                    result.Where(x => x.Descricao.ToUpper().Contains(cat.Descricao.ToUpper()));
                if (cat.NomeFornecedor is not null && !cat.NomeFornecedor.Equals(""))
                    result.Where(x => x.NomeFornecedor.Contains(cat.NomeFornecedor));
                if (cat.NomeCategoria is not null && !cat.NomeCategoria.Equals(""))
                    result.Where(x => x.NomeCategoria.Contains(cat.NomeCategoria));


                var a = result.Skip(10 * (pagina - 1)).Take(10).ToList();
                return a;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void ExcluirProduto(Catalogo catalogo)
        {
            var cat = _context.Catalogo.Where(x => x.IdProduto == catalogo.IdProduto).FirstOrDefault();
            _context.Catalogo.Remove(cat);
            _context.SaveChanges();
        }

        public List<Catalogo> GetFornecedores(Catalogo catalogo)
            => _context.Catalogo.Where(x => x.IdFornecedor == catalogo.IdFornecedor).ToList();

        public List<Catalogo> GetCategoria(Catalogo catalogo)
            => _context.Catalogo.Where(x => x.IdCategoria == catalogo.IdCategoria).ToList();

        public void AtualizaCatalogo(Catalogo catalogo)
        {
            _context.Catalogo.Update(catalogo);
            _context.SaveChanges();
        }
    }
}
