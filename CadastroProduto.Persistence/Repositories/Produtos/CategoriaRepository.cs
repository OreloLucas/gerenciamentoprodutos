﻿using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Interfaces.Produtos;
using CadastroProduto.Infra.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Infra.Repositories.Produtos
{
    public class CategoriaRepository : ICategoriaRepository
    {
        private readonly ProdutosContext _context;

        public CategoriaRepository(ProdutosContext context)
        {
            _context = context;
        }

        public bool Editar(Categoria Categoria)
        {
            try
            {
                _context.Categoria.Update(Categoria);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Excluir(Categoria Categoria)
        {
            try
            {
                _context.Categoria.Remove(Categoria);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Categoria>? Lista(int pagina)
        {
            try
            {
                _context.lazyload = false;
                return _context.Categoria.Skip(10 * (pagina - 1)).Take(10).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Categoria>? ListAll()
        {
            try
            {
                _context.lazyload = true;
                return _context.Categoria.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Categoria? Adicionar(Categoria Categoria)
        {
            try
            {
                _context.Categoria.Add(Categoria);
                _context.SaveChanges();
                return Categoria;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void Detached(Categoria c)
        {
            try
            {
                _context.Entry(c).State = EntityState.Detached;
            }
            catch (Exception)
            {
            }
        }

        public Categoria? Visualizar(int id)
        {
            try
            {
                _context.lazyload = true;
                return _context.Categoria.Where(x => x.Id == id).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
