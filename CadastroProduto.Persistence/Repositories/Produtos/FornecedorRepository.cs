﻿using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Interfaces.Produtos;
using CadastroProduto.Infra.Context;
using Microsoft.EntityFrameworkCore;

namespace CadastroProduto.Infra.Repositories.Produtos
{
    public class FornecedorRepository : IFornecedorRepository
    {
        private readonly ProdutosContext _context;

        public FornecedorRepository(ProdutosContext context)
        {
            _context = context;
        }

        public bool Editar(Fornecedor Fornecedor)
        {
            try
            {
                _context.Fornecedor.Update(Fornecedor);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Excluir(Fornecedor Fornecedor)
        {
            try
            {
                _context.Fornecedor.Remove(Fornecedor);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Fornecedor>? Lista(int pagina)
        {
            try
            {
                _context.lazyload = false;
                return _context.Fornecedor.Skip(10 * (pagina - 1)).Take(10).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Fornecedor>? ListAll()
        {
            try
            {
                _context.lazyload = true;
                return _context.Fornecedor.ToList();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Fornecedor? Adicionar(Fornecedor Fornecedor)
        {
            try
            {
                _context.Fornecedor.Add(Fornecedor);
                _context.SaveChanges();
                return Fornecedor;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void Detached(Fornecedor c)
        {
            try
            {
                _context.Entry(c).State = EntityState.Detached;
            }
            catch (Exception)
            {
            }
        }

        public Fornecedor? Visualizar(int id)
        {
            try
            {
                _context.lazyload = true;
                return _context.Fornecedor.Where(x => x.Id == id).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
