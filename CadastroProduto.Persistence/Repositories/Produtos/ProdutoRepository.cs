﻿using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Interfaces.Produtos;
using CadastroProduto.Infra.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadastroProduto.Infra.Repositories.Produtos
{
    public class ProdutoRepository : IProdutoRepository
    {
        private readonly ProdutosContext _context;

        public ProdutoRepository(ProdutosContext context)
        {
            _context = context;
        }

        public bool Editar(Produto produto)
        {
            try
            {
                _context.Produto.Update(produto);
                _context.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Excluir(Produto produto)
        {
            try
            {
                _context.Produto.Remove(produto);
                _context.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Produto>? Lista(int pagina)
        {
            try
            {
                _context.lazyload = false;
                return _context.Produto.Skip(10 * (pagina - 1)).Take(10).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Produto>? ListAll()
        {
            try
            {
                _context.lazyload = true;
                return _context.Produto
                    
                    .ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Produto? Adicionar(Produto produto)
        {
            try
            {
                _context.Produto.Add(produto);
                _context.SaveChanges();
                return produto;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public void Detached(Produto p)
        {
            try
            {
                _context.Entry(p).State = EntityState.Detached;
            }
            catch (Exception)
            { 
            }
        }

        public Produto? Visualizar(int id)
        {
            try
            {
                _context.lazyload = true;
                return _context.Produto.Where(x => x.Id == id).FirstOrDefault();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
