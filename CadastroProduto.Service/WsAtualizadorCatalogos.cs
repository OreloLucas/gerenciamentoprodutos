﻿using CadastroProduto.Domain.Entities.Catalogo;
using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Ws;
using Newtonsoft.Json;
using System.Text;

namespace CadastroProduto.Service
{
    public class WsAtualizadorCatalogos : IWsAtualizadorCatalogos
    {
        private bool AtualizaCatalogo(string url, StringContent stringContent)
        {
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("https://localhost:7171/");
                client.PostAsync(url, stringContent);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool AtualizaCategoria(Catalogo categoria)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(categoria), Encoding.UTF8, "application/json");
            return AtualizaCatalogo("api/Catalogo/AtualizaCategoria", stringContent);
        }

        public bool AtualizaFornecedor(Catalogo fornecedor)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(fornecedor), Encoding.UTF8, "application/json");
            return AtualizaCatalogo("api/Catalogo/AtualizaFornecedor", stringContent);
        }

        public bool AtualizaProduto(Catalogo produto)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(produto), Encoding.UTF8, "application/json");
            return AtualizaCatalogo("/api/Catalogo/AtualizaProduto", stringContent);
        }

        public bool ExcluirProduto(Catalogo produto)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(produto), Encoding.UTF8, "application/json");
            return AtualizaCatalogo("api/Catalogo/ExcluirProduto", stringContent);
        }

    }
}