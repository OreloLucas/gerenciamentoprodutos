﻿using CadastroProduto.Application.Models;
using CadastroProduto.Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace Cadastrocategoria.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CategoriaController : ControllerBase
    {
        private readonly ICategoriaService _categoriaService;
        private readonly IAtualizadorCatalogoService _atualizadorCatalogo; 

        public CategoriaController(ICategoriaService categoriaService,
            IAtualizadorCatalogoService atualizadorCatalogo)
        {
            _categoriaService = categoriaService;
            _atualizadorCatalogo = atualizadorCatalogo;
        }

        [HttpGet, Route("Lista/{pagina}")]
        public IActionResult Lista(int pagina)
        {
            var result = _categoriaService.Lista(pagina);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }

        [HttpPost, Route("Adicionar")]
        public IActionResult Adicionar(CategoriaViewModel categoria)
        {
            var result = _categoriaService.Adicionar(categoria);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }

        [HttpPost, Route("Editar")]
        public IActionResult Editar(CategoriaViewModel categoria)
        {
            var result = _categoriaService.Editar(categoria);
            return result.Sucesso ? Ok(result) : BadRequest(result);
            //if (result.Sucesso)
            //{
            //    _atualizadorCatalogo.AtualizaCategoria(new CatalogoViewModel()
            //    {
            //        NomeCategoria = result.obj.Nome,
            //        IdCategoria = result.obj.Id,
            //    });
            //    return Ok(result);
            //}
            //else
            //    return BadRequest(result);
        }

        [HttpDelete, Route("Excluir")]
        public IActionResult Excluir(CategoriaViewModel categoria)
        {
            var result = _categoriaService.Excluir(categoria);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }

        [HttpPost, Route("Visualizar")]
        public IActionResult Visualizar(CategoriaViewModel categoria)
        {
            var result = _categoriaService.Visualizar(categoria);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }
    }
}
