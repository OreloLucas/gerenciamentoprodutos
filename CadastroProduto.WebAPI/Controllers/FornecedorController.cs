﻿using CadastroProduto.Application.Models;
using CadastroProduto.Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace CadastroProduto.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class FornecedorController : ControllerBase
    {
        private readonly IFornecedorService _fornecedorService;
        private readonly IAtualizadorCatalogoService _atualizadorCatalogo;

        public FornecedorController(IFornecedorService fornecedorService,
            IAtualizadorCatalogoService atualizadorCatalogo)
        {
            _fornecedorService = fornecedorService;
            _atualizadorCatalogo = atualizadorCatalogo;
        }

        [HttpGet, Route("Lista/{pagina}")]
        public IActionResult Lista(int pagina)
        {
            var result = _fornecedorService.Lista(pagina);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }

        [HttpPost, Route("Adicionar")]
        public IActionResult Adicionar(FornecedorViewModel fornecedor)
        {
            var result = _fornecedorService.Adicionar(fornecedor);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }

        [HttpPost, Route("Editar")]
        public IActionResult Editar(FornecedorViewModel fornecedor)
        {
            var result = _fornecedorService.Editar(fornecedor);
            return result.Sucesso ? Ok(result) : BadRequest(result);
            //if (result.Sucesso)
            //{
            //    _atualizadorCatalogo.AtualizaFornecedor(new CatalogoViewModel()
            //    {
            //        NomeFornecedor = result.obj.Nome,
            //        IdFornecedor = result.obj.Id,
            //    });
            //    return Ok(result);
            //}
            //else
            //    return BadRequest(result);
        }

        [HttpDelete, Route("Excluir")]
        public IActionResult Excluir(FornecedorViewModel fornecedor)
        {
            var result = _fornecedorService.Excluir(fornecedor);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }

        [HttpPost, Route("Visualizar")]
        public IActionResult Visualizar(FornecedorViewModel fornecedor)
        {
            var result = _fornecedorService.Visualizar(fornecedor);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }
    }
}
