﻿using Microsoft.AspNetCore.Mvc;

namespace CadastroProduto.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class MonitoramentoController : ControllerBase
    {
        [HttpGet, Route("Check")]
        public IActionResult Check()
        {
           return Ok("ok");
        }
    }
}
