﻿using CadastroProduto.Application.Models;
using CadastroProduto.Application.Services;
using CadastroProduto.Domain.Entities.Produtos;
using CadastroProduto.Domain.Enum;
using CadastroProduto.Domain.Ws;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;
using System.Text.Json.Serialization;

namespace CadastroProduto.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProdutoController : ControllerBase
    {
        private readonly IProdutoService _produtoService;       

        public ProdutoController(IProdutoService produtoService,
            IAtualizadorCatalogoService atualizadorCatalogo )
        { 
            _produtoService = produtoService;        
        }

        [HttpGet, Route("Lista/{pagina}")]
        public IActionResult Lista(int pagina)
        {
            var result = _produtoService.Lista(pagina);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }

        [HttpPost, Route("Adicionar")]
        public IActionResult Adicionar(ProdutoViewModel produto)
        {
            var result = _produtoService.Adicionar(produto);
            return result.Sucesso ? Ok(result.obj) : BadRequest(result);
        }

        [HttpPost, Route("Editar")]
        public IActionResult Editar(ProdutoViewModel produto)
        {
            var result = _produtoService.Editar(produto);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }

        [HttpDelete, Route("Excluir")]
        public IActionResult Excluir(ProdutoViewModel produto)
        {
            var result = _produtoService.Excluir(produto);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }

        [HttpPost, Route("Visualizar")]
        public IActionResult Visualizar(ProdutoViewModel produto)
        {
            var result = _produtoService.Visualizar(produto);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }
    }
}
