using CadastroProduto.Infra.Context;
using Microsoft.EntityFrameworkCore;
using CadastroProduto.Application.Services;
using CadastroProduto.Domain.Interfaces.Produtos;
using CadastroProduto.Infra.Repositories.Produtos;
using CadastroCategoria.Application.Services;
using CadastroProduto.Domain.Ws;
using CadastroProduto.Service;
using CadastroProduto.Application.Interfaces;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddScoped<IProdutoService, ProdutoService>();
builder.Services.AddScoped<IProdutoRepository, ProdutoRepository>();

builder.Services.AddScoped<IFornecedorService, FornecedorService>();
builder.Services.AddScoped<IFornecedorRepository, FornecedorRepository>();

builder.Services.AddScoped<ICategoriaService, CategoriaService>();
builder.Services.AddScoped<ICategoriaRepository, CategoriaRepository>();
builder.Services.AddScoped<IAtualizadorCatalogoService, AtualizadorCatalogoService>();
builder.Services.AddScoped<IWsAtualizadorCatalogos, WsAtualizadorCatalogos>();
builder.Services.AddScoped<ISendMessageService, SendMessageService>();
//builder.Services.AddScoped<IReciveMessageService, ReciveMessageService>();

IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();

builder.Services.AddDbContext<ProdutosContext>(
    o =>
    {
        o.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
    });



var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
