﻿using CadastroProduto.Application.Models;
using CadastroProduto.Application.Services;
using Microsoft.AspNetCore.Mvc;

namespace CatalogoProduto.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CatalogoController : ControllerBase
    {
        private readonly ICatalogoService _catalogoService;

        public CatalogoController(ICatalogoService catalogoService)
        {
            _catalogoService = catalogoService;
        }

        [HttpPost, Route("Lista")]
        public IActionResult Lista(CatalogoFiltroViewModel CatViewModel)
        {
            var result = _catalogoService.Lista(CatViewModel);
            return result.Sucesso ? Ok(result) : BadRequest(result);
        }
    }
}
