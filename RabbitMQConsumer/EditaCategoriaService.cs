﻿using CadastroProduto.Application.Interfaces;
using CadastroProduto.Application.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Text;


namespace RabbitMQConsumer
{

    public class EditaCategoriaService : BackgroundService
    {
        private readonly IReciveMessageService _reciveMessage;
        private readonly ILogger _logger;
        private const string QUEUE_NAME = "atualiza-categoria";

        public EditaCategoriaService(IReciveMessageService reciveMessage, ILogger<EditaCategoriaService> logger)
        {
            _reciveMessage = reciveMessage;
            _logger = logger;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                return Task.Run(() =>
                {
                    while(true)
                    {
                        try
                        {
                            ConnectionFactory connectionFactory = new ConnectionFactory()
                            {
                                HostName = "localhost",
                                NetworkRecoveryInterval = TimeSpan.FromSeconds(10),
                                AutomaticRecoveryEnabled = true,   

                            };

                            var connection = connectionFactory.CreateConnection(); 
                            var channel = connection.CreateModel();

                            channel.QueueDeclare(queue: QUEUE_NAME,
                                            durable: true,
                                            exclusive: false,
                                            autoDelete: false,
                                            arguments: null);

                            BasicGetResult result = channel.BasicGet(QUEUE_NAME, true);
                            if (result is not null)
                            {
                                IBasicProperties props = result.BasicProperties;
                                var message = Encoding.UTF8.GetString(result.Body.ToArray());
                                var catalogo = JsonConvert.DeserializeObject<CatalogoViewModel>(message);
                                _reciveMessage.AtualizaCategoria(catalogo);
                            }
                            Task.Delay(5000).Wait();
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex.Message);
                            _logger.LogError(ex.InnerException?.Message);
                        }
                    }
                }, stoppingToken);
            }
            catch (Exception ex)
            {
                return Task.FromException(ex);
            }
        }
    }
}