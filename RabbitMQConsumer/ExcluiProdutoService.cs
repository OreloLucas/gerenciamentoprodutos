﻿using CadastroProduto.Application.Interfaces;
using CadastroProduto.Application.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System.Text;

namespace RabbitMQConsumer
{

    public class ExcluiProdutoService : BackgroundService
    {
        private readonly IReciveMessageService _reciveMessage;
        private readonly ILogger _logger;
        private const string QUEUE_NAME = "exclui-produto";
        public ExcluiProdutoService(IReciveMessageService reciveMessage, ILogger<ExcluiProdutoService> logger)
        {
            _reciveMessage = reciveMessage;
            _logger = logger;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
               return Task.Run(async () =>
                {
                    while(true)
                    {
                        try
                        {
                            ConnectionFactory connectionFactory = new ConnectionFactory()
                            {
                                HostName = "localhost",
                            };

                            var connection = connectionFactory.CreateConnection();
                            var channel = connection.CreateModel();

                            channel.QueueDeclare(queue: QUEUE_NAME,
                                            durable: true,
                                            exclusive: false,
                                            autoDelete: false,
                                            arguments: null);

                            BasicGetResult result = channel.BasicGet(QUEUE_NAME, true);
                            if (result is not null)
                            {
                                IBasicProperties props = result.BasicProperties; 
                                var message = Encoding.UTF8.GetString(result.Body.ToArray());
                                var catalogo = JsonConvert.DeserializeObject<CatalogoViewModel>(message);
                                _reciveMessage.ExcluirProduto(catalogo);
                            }
                            Task.Delay(5000).Wait();
                        }
                        catch (Exception ex)
                        {
                            _logger.LogError(ex.Message);
                            _logger.LogError(ex.InnerException?.Message);
                        }
                    }
                    
                }, stoppingToken);
              
            }
            catch (Exception ex)
            {
                return Task.FromException(ex);
            }            
        }
    }
}