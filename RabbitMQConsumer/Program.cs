﻿using CadastroProduto.Application.Interfaces;
using CadastroProduto.Application.Services;
using CadastroProduto.Domain.Interfaces.Produtos;
using CadastroProduto.Infra.Context;
using CadastroProduto.Infra.Repositories.Produtos;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RabbitMQConsumer;
using System.Configuration;

var host = Host.CreateDefaultBuilder(args).ConfigureServices((context, services) =>
{ 

    services.AddScoped<ICatalogoRepository, CatalogoRepository>();
    services.AddScoped<IReciveMessageService, ReciveMessageService>();

    IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();
    services.BuildServiceProvider();
   
    services.AddDbContext<CatalogoContext>(
    o =>
    {
        o.UseNpgsql(configuration.GetConnectionString("DefaultConnection"));
    });
    services.AddLogging(); 
    services.AddHostedService<AtualizaProdutoService>();
    services.AddHostedService<ExcluiProdutoService>();
    services.AddHostedService<EditaCategoriaService>();
    services.AddHostedService<EditaFornecedorService>();
}).Build();

host.Start();
host.Run();