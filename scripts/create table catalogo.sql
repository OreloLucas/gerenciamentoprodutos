CREATE TABLE public.catalogo (
	"Id" serial,
	Descricao varchar NULL,	
	IdFornecedor int4 NOT NULL,
	NomeFornecedor int4 NOT NULL,
	IdCategoria int4 NULL,
	NomeCategoria varchar NULL,	
	PrecoVenda money NULL,
	CONSTRAINT produto_pkey PRIMARY KEY ("Id")
);