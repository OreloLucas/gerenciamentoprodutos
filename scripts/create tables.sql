CREATE TABLE public.categoria (
	"Id" serial,
	nome varchar NULL,
	CONSTRAINT categoria_pkey PRIMARY KEY ("Id")
);

CREATE TABLE public.fornecedor (
	"Id" serial,
	nome varchar NULL,
	CONSTRAINT fornecedor_pkey PRIMARY KEY ("Id")
);

 

CREATE TABLE public.produto (
	"Id" serial,
	descricao varchar NULL,
	categoria int4 NULL,
	tipo int4 NULL,
	preco_venda money NULL,
	preco_custo money NULL,
	id_fornecedor int4 NOT NULL,
	CONSTRAINT produto_pkey PRIMARY KEY ("Id")
);


-- public.produto foreign keys

ALTER TABLE public.produto ADD CONSTRAINT produto_id_fornecedor_fkey FOREIGN KEY (id_fornecedor) REFERENCES public.fornecedor("Id");

ALTER TABLE public.produto ADD CONSTRAINT produto_id_categoria_fkey FOREIGN KEY (categoria) REFERENCES public.categoria("Id");